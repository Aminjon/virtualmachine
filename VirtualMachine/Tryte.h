#include <iostream>
#include <string>
#include <stdexcept>

class Tryte
{
public:
    Tryte();
    Tryte(const std::string &value );

    unsigned char trit( unsigned char pos ) const;
    void setTrit( unsigned char pos, unsigned char value );

    Tryte operator+(const Tryte& b);
    Tryte operator*(const Tryte& b);
    Tryte operator-(const Tryte& b);
    Tryte operator/(const Tryte& b);

    bool operator==(const Tryte& b);
    bool operator!=(const Tryte& b);
    bool operator< (const Tryte& b);
    bool operator<=(const Tryte& b);
    bool operator> (const Tryte& b);
    bool operator>=(const Tryte& b);

public:

    friend std::ostream& operator<<(std::ostream& os, const Tryte& b)
    {
        os << (int)b.t0;
        os << (int)b.t1;
        os << (int)b.t2;
        os << (int)b.t3;
        os << (int)b.t4;
        os << (int)b.t5;

        return os;
    }

private:
    unsigned char t0:2;
    unsigned char t1:2;
    unsigned char t2:2;
    unsigned char t3:2;
    unsigned char t4:2;
    unsigned char t5:2;
};