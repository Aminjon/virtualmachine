#include <cmath>
#include "Tryte.h"

class Int
{
public:
    Int(){ this->_value = new Tryte[4]; }
    Int( const std::string& value );
    Int( const Int& value );

    Int operator=(const Int& b);
    Int operator+(const Int& b);
    Int operator*(const Int& b);
    Int operator-(const Int& b);
    Int operator/(const Int& b);

    bool operator==(const Int& b);
    bool operator!=(const Int& b);
    bool operator< (const Int& b);
    bool operator> (const Int& b);
    bool operator>=(const Int& b);
    bool operator<=(const Int& b);

private:
    unsigned char trit(unsigned i) const;
    void setTrit(unsigned char pos, unsigned char value);
    std::string sign (const std::string& value);
    static Tryte* make (const std::string& value);

protected:
    friend std::ostream& operator<<(std::ostream& os, const Int& b)
    {
        Int newInt = b;

        Int median;
        median._value = Int::make("111111111111111111111111");

        if(newInt > median)
        {
            os << '-';

            Int one;
            one._value = Int::make("1");

            newInt = (median + one) - (newInt - median);
        }

        bool prefix = true;
        for(int i = 0; i < 24; i++)
        {
            if( prefix && int(newInt.trit(i)) != 0  )
                prefix = false;

            if( i == 24 )
                os << '.';

            if( !(int(newInt.trit(i)) == 0 && prefix) )
                os << int(newInt.trit(i));
        }

        return os;
    }

protected:
    Tryte * _value;
};