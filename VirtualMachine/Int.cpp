#include "Int.h"

Int::Int(const std::string &value)
{
    std::string v = value;
    if(v.size() > 1)
    {
        if(v.at(0) == '-')
        {
            v = sign(v);
        }
    }

    this->_value = make(v);
}

Int::Int(const Int &value)
{
    std::string v = "";
    for(unsigned i = 0; i < 24; i++)
    {
        v += value.trit(i) + '0';
    }

    this->_value = make(v);
}

Int Int::operator=(const Int &b)
{
    std::string v = "";
    for(unsigned i = 0; i < 24; i++)
    {
        v += b.trit(i) + '0';
    }

    this->_value = make(v);

    return *this;
}

Int Int::operator+(const Int &b)
{
    Int newInt("0");

    unsigned char newTrit = 0;
    unsigned char overTrit = 0;
    for(int i = 6*4-1; i >= 0; i--)
    {
        unsigned char left = this->trit(i);
        unsigned char right = b.trit(i);

        newTrit = (left + right + overTrit) % 3;
        overTrit = (left + right + overTrit) / 3;

        newInt.setTrit(i, newTrit);
    }

    return newInt;
}

Int Int::operator*(const Int &b)
{
    Int newInt("0");

    Int counter = b;
    Int zero("0");
    Int one("1");

    while( counter > zero )
    {
        newInt = newInt + *this;
        counter = counter - one;
    }

    return newInt;
}

Int Int::operator-(const Int &b)
{
    Int newInt("0");

    unsigned char overTrit = 0;
    for(int i = 6 * 4- 1; i >= 0; i--)
    {
        unsigned char left = this->trit(i);
        unsigned char right = b.trit(i);
        unsigned char newTrit = 0;

        if( (left - overTrit) < right )
        {
            newTrit = (left - overTrit + 3) - right;
            overTrit = 1;
        }
        else
        {
            newTrit = left - right - overTrit;
            overTrit = 0;
        }

        newInt.setTrit(i, newTrit);
    }

    return newInt;
}

Int Int::operator/(const Int &b)
{
    if( *this < b )
        return Int();

    Int newInt("0");

    Int one("1");
    Int temp = *this;

    while( temp >= b )
    {
        temp = temp - b;
        newInt = newInt + one;
    }

    return newInt;
}

bool Int::operator==(const Int &b)
{
    for(unsigned i = 0; i < 6 * 4; i++)
    {
        unsigned char left = this->trit(i);
        unsigned char right = b.trit(i);

        if( left != right )
            return false;
    }

    return true;
}

bool Int::operator!=(const Int &b)
{
    for(unsigned i = 0; i < 6 * 4; i++)
    {
        unsigned char left = this->trit(i);
        unsigned char right = b.trit(i);

        if( left != right )
            return true;
    }

    return false;
}

bool Int::operator<(const Int &b)
{
    for(unsigned i = 0; i < 6 * 4; i++)
    {
        unsigned char left = this->trit(i);
        unsigned char right = b.trit(i);

        if( left < right )
            return true;
        else if( left > right )
            return false;
    }

    return false;
}

bool Int::operator>(const Int &b)
{
    for(unsigned i = 0; i < 6 * 4; i++)
    {
        unsigned char left = this->trit(i);
        unsigned char right = b.trit(i);

        if( left > right )
            return true;
        else if( left < right )
            return false;
    }

    return false;
}

bool Int::operator>=(const Int &b)
{
    for(unsigned i = 0; i < 6 * 4; i++)
    {
        unsigned char left = this->trit(i);
        unsigned char right = b.trit(i);

        if( left > right )
            return true;
        else if( left < right )
            return false;
    }

    return true;
}

bool Int::operator<=(const Int &b)
{
    for(int i = 0; i < 6 * 4; i++)
    {
        unsigned char left = this->trit(i);
        unsigned char right = b.trit(i);

        if( left < right )
            return true;
        else if( left > right )
            return false;
    }

    return true;
}

unsigned char Int::trit(unsigned i) const
{
    if( i > 5 )
        return this->_value[i / 5].trit(i % 5);
    else
        return this->_value[0].trit(i);
}

void Int::setTrit(unsigned char pos, unsigned char value)
{
    if( pos > 5 )
        this->_value[pos / 5].setTrit(pos % 5, value);
    else
        this->_value[0].setTrit(pos, value);
}

std::string Int::sign(const std::string &value)
{
    std::string v = "";
    for(unsigned i = 1; i < value.size(); i++)
    {
        v += value.at(i);
    }

    Int newInt;
    newInt._value = make(v);

    Int one;
    one._value = make("1");

    Int median;
    median._value = make("111111111111111111111111");

    Int temp = median + one;
    Int temp2 = temp - newInt;
    Int temp3 = median + temp2;

    std::string nvalue = "";
    for(unsigned i = 0; i < 6 * 4; i++)
        nvalue += temp3.trit(i);

    return nvalue;
}

Tryte* Int::make(const std::string &value)
{
    std::string v = value;
    while( v.size() < 24)
    {
        v = "0" + v;
    }

    Tryte * temp = new Tryte[4];
    for(unsigned i = 0; i < v.size() && i < 24; i++)
    {
        if( i > 5 )
            temp[i / 5].setTrit(i % 5, v.at(i) - '0');
        else
            temp[0].setTrit(i, v.at(i) - '0');
    }

    return temp;
}
