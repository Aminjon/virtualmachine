#include <iostream>
#include <string>
#include "Int.h"

using namespace std;

int main()
{
    string a = "";
    cout << "Left arg: ";
    cin >> a;

    string b = "";
    cout << "Right arg: ";
    cin >> b;

    Int aa(a);
    Int bb(b);

    string op = "";
    cout << "What operations? ";
    cin >> op;

    if(op == "+")
        cout << aa + bb << endl;
    if(op == "-")
        cout << aa - bb << endl;
    if(op == "*")
        cout << aa * bb << endl;
    if(op == "/")
        cout << aa / bb << endl;

	system("pause");

    return 0;
}
