#include "Tryte.h"

Tryte::Tryte()
{
    for(int i = 0; i < 6; i++)
    {
        this->setTrit(i, 0);
    }
}

Tryte::Tryte(const std::string &value)
{
    std::string v = value;
    while( v.size() < 6)
    {
        v = "0" + v;
    }

    for(int i = 0; i < v.size(); i++)
    {
        this->setTrit(i, v.at(i) - '0');
    }
}

unsigned char Tryte::trit(unsigned char pos) const
{
    if( pos > 5 )
        throw std::invalid_argument("pos > 5");

    if(pos == 0)        return t0;
    else if(pos == 1)   return t1;
    else if(pos == 2)   return t2;
    else if(pos == 3)   return t3;
    else if(pos == 4)   return t4;
    else if(pos == 5)   return t5;

    return 0;
}

void Tryte::setTrit(unsigned char pos, unsigned char value)
{
    if(pos == 0)        t0 = value;
    else if(pos == 1)   t1 = value;
    else if(pos == 2)   t2 = value;
    else if(pos == 3)   t3 = value;
    else if(pos == 4)   t4 = value;
    else if(pos == 5)   t5 = value;
}

Tryte Tryte::operator+(const Tryte &b)
{
    Tryte newTryte;
    unsigned char overTrit = 0;

    for(int i = 5; i >= 0; i--)
    {
        unsigned char newTrit = (this->trit(i) + b.trit(i) + overTrit) % 3;
        newTryte.setTrit(i, newTrit);
        overTrit = (this->trit(i) + b.trit(i) + overTrit) / 3;
    }

    return newTryte;
}

Tryte Tryte::operator*(const Tryte &b)
{
    Tryte newTryte;

    Tryte counter = b;
    while( counter > Tryte("0") )
    {
        newTryte = newTryte + *this;
        counter = counter - Tryte("1");
    }

    return newTryte;
}

Tryte Tryte::operator-(const Tryte &b)
{
    Tryte newTryte;

    unsigned char overTrit = 0;
    for(int i = 5; i >= 0; i--)
    {
        unsigned char newTrit = 0;
        if( (this->trit(i) - overTrit) < b.trit(i) )
        {
            newTrit = (this->trit(i) - overTrit + 3) - b.trit(i);
            overTrit = 1;
        }
        else
        {
            newTrit = this->trit(i) - b.trit(i) - overTrit;
            overTrit = 0;
        }

        newTryte.setTrit(i, newTrit);
    }

    return newTryte;
}

Tryte Tryte::operator/(const Tryte &b)
{
    if( (*this) < b )
        return Tryte("0");

    Tryte newTryte;

    Tryte temp = *this;
    while( temp >= b )
    {
        temp = temp - b;
        newTryte = newTryte + Tryte("1");
    }

    return newTryte;
}

bool Tryte::operator==(const Tryte &b)
{
    for(int i = 0; i < 6; i++)
    {
        if( this->trit(i) != b.trit(i) )
            return false;
    }

    return true;
}

bool Tryte::operator!=(const Tryte &b)
{
    for(int i = 0; i < 6; i++)
    {
        if( this->trit(i) != b.trit(i) )
            return true;
    }

    return false;
}

bool Tryte::operator<(const Tryte &b)
{
    for(int i = 0; i < 6; i++)
    {
        if( this->trit(i) < b.trit(i) )
            return true;
        else if( this->trit(i) > b.trit(i) )
            return false;
    }

    return false;
}

bool Tryte::operator<=(const Tryte &b)
{
    for(int i = 0; i < 6; i++)
    {
        if( this->trit(i) < b.trit(i) )
            return true;
        else if( this->trit(i) > b.trit(i) )
            return false;
    }

    return true;
}

bool Tryte::operator>(const Tryte &b)
{
    for(int i = 0; i < 6; i++)
    {
        if( this->trit(i) > b.trit(i) )
            return true;
        else if( this->trit(i) < b.trit(i) )
            return false;
    }

    return false;
}

bool Tryte::operator>=(const Tryte &b)
{
    for(int i = 0; i < 6; i++)
    {
        if( this->trit(i) > b.trit(i) )
            return true;
        else if( this->trit(i) < b.trit(i) )
            return false;
    }

    return true;
}